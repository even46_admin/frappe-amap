<?php
declare(strict_types=1);

use frappe\amap\facade\FrappeAmapRegion;

if (!function_exists('frappe_amap_region_name_by_code')) {
    /**
     * 通过行政编码获取名称
     * @param $code
     * @param bool $cache
     * @return string
     * @throws Exception
     * @author yinxu
     * @date 2024/4/2 14:36:19
     */
    function frappe_amap_region_name_by_code($code, bool $cache = true): string
    {
        $region = frappe_amap_region_by_code($code, $cache) ?? [];
        return $region['name'] ?? "";
    }
}

if (!function_exists('frappe_amap_region_by_code')) {
    /**
     * 通过行政编码获取区域信息
     * @param $code
     * @param bool $cache
     * @return array
     * @throws Exception
     * @author yinxu
     * @date 2024/4/2 14:36:19
     */
    function frappe_amap_region_by_code($code, bool $cache = true): array
    {
        $region = $cache ? (array) cache("amap_region_by_code_$code") : [];
        if (empty($region)) {
            $regions = frappe_amap_regions(true);
            foreach ($regions as $item) {
                if ($item['id'] == $code) {
                    $region = $item;
                    break;
                }
            }
            if (!empty($region) && $cache) {
                cache("amap_region_by_code_$code", $region, 30*24*60*60);
            }
        }
        return $region;
    }
}

if (!function_exists('frappe_amap_regions')) {
    /**
     * 获取全部区域
     * @param bool $cache
     * @return array
     * @throws Exception
     * @author yinxu
     * @date 2024/3/25 11:52:06
     */
    function frappe_amap_regions(bool $cache = true): array
    {
        if ($cache) $regions = (array) cache('amap_regions') ?? [];
        if (empty($regions)) {
            $regions = FrappeAmapRegion::regions();
            if ($cache) {
                cache('amap_regions', $regions, 30*24*60*60);
            }
        }
        if (!$cache) cache('amap_regions', null);
        return $regions;
    }
}

if (!function_exists('frappe_amap_tree_regions')) {
    /**
     * 获取全部区域Tree
     * @param bool $cache
     * @return array
     * @throws Exception
     * @author yinxu
     * @date 2024/3/25 11:52:06
     */
    function frappe_amap_tree_regions(bool $cache = true): array
    {
        if ($cache) $regions = (array) cache('amap_tree_regions') ?? [];
        if (empty($regions)) {
            $regions = FrappeAmapRegion::treeRegions();
            if ($cache) {
                cache('amap_tree_regions', $regions, 30*24*60*60);
            }
        }
        if (!$cache) cache('amap_tree_regions', null);
        return $regions;
    }
}