<?php
// +----------------------------------------------------------------------
// | Region配置文件
// +----------------------------------------------------------------------

return [
    // 高德api地址
    'key' => env('AMAP_KEY', ''),
    'region' => [
        'url' => env('AMAP_REGION_URL', '')
    ],
];